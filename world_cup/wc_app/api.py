from tastypie.authorization import Authorization
from tastypie import fields
from tastypie.resources import ModelResource
from wc_app.models import *
from wc_app.prettyPrint import *

class CountryResource(ModelResource):
    related_cultures = fields.ManyToManyField('self', 'related_cultures', related_name='country',  blank=True, null=True)

    class Meta:
        queryset = Country.objects.all()
        resource_name = 'countries'
        authorization = Authorization()
        serializer = PrettyJSONSerializer()

    def determine_format(self, request):
        return "application/json"


class PlayerResource(ModelResource):
    related_cultures = fields.ManyToManyField('self', 'related_cultures', related_name='cultures',  blank=True, null=True)

    class Meta:
        queryset = Player.objects.all()
        resource_name = 'players'
        authorization = Authorization()
        serializer = PrettyJSONSerializer()

    def determine_format(self, request):
        return "application/json"


class MatchResource(ModelResource):
    related_cultures = fields.ManyToManyField('self', 'related_cultures', related_name='cultures',  blank=True, null=True)

    class Meta:
        queryset = Match.objects.all()
        resource_name = 'matches'
        authorization = Authorization()
        serializer = PrettyJSONSerializer()

    def determine_format(self, request):
        return "application/json"